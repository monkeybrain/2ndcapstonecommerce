const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({

	email: {
		type: String,
		required: [true, "Email is required"]
	},

	password: {
		type: String,
		required: [true, "Password is required"]
	},

	isAdmin: {
		type: Boolean,
		default: false
	},

	orders: [
		{
			productId: {
				type: String,
				required: [true, "Please enter the ID of the product"]
			},

			totalAmount: {
				type: Number,
				required: [true, "Please input the amount ordered"]
			},

			purchasedOn: {
				type: Date,
				default: new Date()
			}

		}]



});

module.exports = mongoose.model("User", userSchema);
