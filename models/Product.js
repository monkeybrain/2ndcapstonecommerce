const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({

	name: {
		type: String,
		required: [true, "Item label is required"]
	},

	description: {
		type: String,
		required: [true, "Item description is required"]
	},

	price: {
		type: Number,
		required: [true, "Item price is required"]
	},

	isActive: {
		type: Boolean,
		default: true
	},

	createdOn: {
		type: Date,
		default: new Date()
	},

	
	
	customerOrders: [
	 
		{
			
			userId: {
				type: String,
				required: [true, "Please input the ID of the user"]
			},

			totalAmount: {
				type: Number,
				required: [true, "Please input the amount ordered"]
			},

			purchasedOn: {
				type: Date,
				default: new Date()
			}

		}]


});

module.exports = mongoose.model("Product", productSchema)
