const express = require("express");
const router = express.Router();
const auth = require("../auth");
const productController = require("../controllers/product");

//Create Product(Admin Only)
router.post("/", auth.verify, (req, res) => 
{
	const productData = auth.decode(req.headers.authorization)

	if (productData.isAdmin === true) 
	{

		productController.createProduct(req.body).then(resultFromController => res.send(`Product Successfully created!`));
	} else {
		res.send(`Unauthorized User, for Admin use only`)
	}


})

//Search all Produts
router.get("/products", (req, res) => 
{
	productController.getActiveProducts().then(resultFromController => res.send(resultFromController));

})

//Search single Product
router.get("/products/:productId", (req, res) => 
{
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController))
})


//Update Specific Product Information(Admin Only)
router.put("/products/:productId", auth.verify, (req, res) => 
{
	const productData = auth.decode(req.headers.authorization)

	if (productData.isAdmin === true) 
	{
		productController.updateProduct(req.params, req.body).then(resultFromController => res.send(`Product Successfully updated!`));
	} else {
		res.send(`Unauthorized User, for Admin use only`)
	}
})

//Archive Product(Admin Only)
router.put("/products/:productId/archive", auth.verify, (req, res) => 
{
	const productData = auth.decode(req.headers.authorization)

	if (productData.isAdmin === true) 
	{
		productController.archiveProduct(req.params, req.body).then(resultFromController => res.send(`Product was Successfully Archived`));
	} else {
		res.send(`Unauthorized User, for Admin user only`)
	}
})


module.exports = router;
