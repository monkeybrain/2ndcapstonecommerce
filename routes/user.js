const express = require("express");
const router = express.Router();
const auth = require("../auth");
const userController = require("../controllers/user");
const productController = require("../controllers/product");

//Register
router.post("/register", (req, res) => 
{
	userController.registerUser(req.body).then(resultFromController => res.send(`You have succesfully created a new user!`))
		console.log(req.body)
})


//LogIn
router.post("/login", (req, res) => 
{
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
	console.log(req.body)
})


//Update
router.put("/:userId/setAsAdmin", auth.verify, (req, res) => 
{
	const userData = auth.decode(req.headers.authorization)

	let data = 
	{
		userId: req.params.userId,
		isAdmin: userData.isAdmin
	}

	if (userData.email === "admin@email.com") 
	{

		if (userData.isAdmin === false) {
			userController.setAdmin(data).then(resultFromController => res.send(resultFromController))
		} else {
			res.send(`The user is already an Admin`)
		}
	} else {
		res.send(`Wrong user`);
	}
});


//Create Order
router.post("/checkout", auth.verify, (req, res) => {

	const isUserLoggedIn = auth.decode(req.headers.authorization)

	if (isUserLoggedIn.isAdmin === false) {
	let data = {
		userId: isUserLoggedIn.id,
		productId: req.body.productId,
		totalAmount: req.body.totalAmount
	}

	userController.checkout(data).then(resultFromController => res.send('You have succesfully created your order!'));
		console.log(resultFromController)
	} else {
		res.send(`Wrong user credentials`)
	}
})





//Get all Orders Admin only

router.get("/orders", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	if (userData.isAdmin === true) {

		userController.getOrders().then(resultFromController => res.send(resultFromController))
	} else {
		res.send(`Wrong user credentials`)	
	}
})




//Get user order non Admin
router.get("/myOrders", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)

	if (userData.isAdmin === false) {

		let data = userData

		userController.getUsersOrders(data).then(resultFromController => res.send(resultFromController))
	} else {
		res.send(`Wrong user credentials`)	
	}

})




module.exports = router;
