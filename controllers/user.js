const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const auth = require("../auth");



//Register
module.exports.registerUser = (reqBody) => 
{
	let newUser = new User ({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10),
	})

	return newUser.save().then((user, error) => 
	{
		if (error) {
			return false
		} else {
			return true
		}
	})

};


//LogIn
module.exports.loginUser = (reqBody) => {
return User.findOne({email: reqBody.email}).then(result => 
{
		if (result == null){
			return false;
		} else {

			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})
};

//Update
module.exports.setAdmin = (data) => 
{
return User.findByIdAndUpdate(data.userId, {isAdmin: true}).then((user, error) => 
	{
		if(error) {
			res.send(`Wrong user authentication`)
		} else {
			return true
		}
	})
};


//Create Order

module.exports.checkout = async (data) => {

	
	let isUserUpdated = await User.findById(data.userId).then(user => {

		let orderData = {

			productId: data.productId,
			totalAmount: data.totalAmount
		}


		user.orders.push(orderData);

		return user.save().then((user, error) => {
			if(error) {
				return false
			} else {
				return true
			}
		})
	})


	let isProductUpdated = await Product.findById(data.productId).then((product, error) => {

		let orderData = {

			userId: data.userId,
			totalAmount: data.totalAmount
		}

		product.customerOrders.push(orderData);

		return product.save().then((product, error) => {
			if(error) {
				return false
			} else {
				return true
			}
		})
	})


	if(isUserUpdated && isProductUpdated) {
		return true
	} else {
		return false
	}

}


//Search all active orders
module.exports.getOrders = () => {
    return Product.find({}).then(result => {
        return result;
    })
}



//Search user orders
module.exports.getUsersOrders = (data) => {

	return User.findById(data.id).then(result => {
		return result.orders
	})
}




