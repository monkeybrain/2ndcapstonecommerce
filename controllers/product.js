const Product = require("../models/Product");
const User = require("../models/User");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const auth = require("../auth");

//Create Product
module.exports.createProduct = (reqBody) => 
{
	let newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price

	})
	return newProduct.save().then((product, error) => 
	{

		if (error) {
			return false
		} else {
			return true
		}
	})

};

//Search all Products
module.exports.getActiveProducts = () => 
{
return Product.find({isActive: true}).then(result => 
{
		return result
	})
};

//Search single Product
module.exports.getProduct = (reqParams) => 
{
	return Product.findById(reqParams.productId).then(result => 
	{
		return result
	})
};

//Update Specific Product
module.exports.updateProduct = (reqParams, reqBody) => 
{
	let updatedProduct = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {
		if(error) {
			return false
		} else {
			return true
		}

	})
};

//Archive Product
module.exports.archiveProduct = (reqParams, reqBody) => 
{
	let archivedProduct = {isActive: false}
	return Product.findByIdAndUpdate(reqParams.productId, archivedProduct).then((product, error) => {
		if(error) {
			return false
		} else {
			return true
		}
	})
};
